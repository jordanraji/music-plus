#!/usr/bin/python

import psycopg2
import redis

# CONECTION POSTGRESQL
conn = psycopg2.connect(database="Music+", user = "raji", password = "2400", host = "127.0.0.1", port = "5432")
print "Opened PostgresSQL database successfully"

# CONECTION REDIS
r = redis.StrictRedis(host='localhost', port=6379, db=0)
print "Opened Redis database successfully"

# value = 123
# if(r.exists("hola") == False):
#     r.sadd("hola", value)
#     print "Key value created"
# else:
#     if(r.sismember("hola", value) == True):
#         print "The value is member"
#     else:
#         r.sadd("hola", value)
#         print "Value added"

cur = conn.cursor()
cur.execute("SELECT id, artist, song, text FROM dataset")

rows = cur.fetchall()
for row in rows:
    my_list = row[3].split()
    for word in my_list:
        # print word, row[0]
        if(r.exists(word) == False):
            r.sadd(word, row[0])
            print "Key & value created"
        else:
            if (r.sismember(word, row[0]) == True):
                print "The value is member"
            else:
                r.sadd(word, row[0])
                print "Value added"

print "Operation done successfully";
conn.close()
