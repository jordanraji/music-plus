# Music+

## Instalación
1. Instalar PostgreSQL https://www.postgresql.org
2. Instalar Redis https://redis.io
3. Iniciar PostgresSQL Server
4. Importar Base de Datos a PostgreSQL 'Music+'
5. Instalar pip https://pip.pypa.io/en/stable/installing/
6. Instalar requisitos

`$ pip install -r requirements.txt`

## Configuración
1. En `PATH_TO/web/config.py` configurar SQLALCHEMY_DATABASE_URI. Más información en http://docs.sqlalchemy.org/en/latest/core/engines.html#postgresql
2. En `PATH_TO/web/main.py` configurar `r = redis.StrictRedis(host='localhost', port=6379, db=0)`

## Uso
* Iniciar PostgresSQL Server.
* Ubicarse en `PATH_TO/Test/dump.rdb`
* Iniciar Redis-Server
* Entrar a `PATH_TO/web` y ejecutar `python main.py`
* Entrar al navegador web http://localhost:5000
* Usar Music+
