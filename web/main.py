# -*- coding: utf-8 -*-
from flask import Flask, render_template, url_for, redirect
from flask import request, make_response
from flask import session
from flask import flash
from flask_wtf.csrf import CSRFProtect

from config import DevelopmentConfig

from sqlalchemy.sql import table, column, select, update, insert
from sqlalchemy import or_, and_
from sqlalchemy import text

from babel import *
from models import db
import forms
import redis

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
csrf = CSRFProtect()



@app.route('/', methods = ['GET','POST'])
def index():
    form = forms.MusicSearchForm()
    result = []
    data = []
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    if request.method == 'POST':
        text = form.text.data
        words = text.split()
        for word in words:
            result = result + list(r.smembers(word))
        result = list(set(result))
        for value in result:
            res = db.engine.execute("select artist, song, link from dataset where id = " + value + ";")
            for row in res:
                data.append(row)
        # for value in data:
        #     print value
    return render_template('base.html', form = form, data = data)

if __name__ == '__main__':
    csrf.init_app(app)
    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.run(host='127.0.0.1', port=5000, threaded=True)
